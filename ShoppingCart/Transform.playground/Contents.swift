import UIKit

let numbers = [0,1,2,3,4]
let names = ["Hillary", "laura", "Michelle", "Melania"]
let sources = ["http://cnn.com", "http://nytimes.com", "invalid"]



extension Sequence {
    func myMap<Transformed>( transform: (Element) throws -> Transformed) rethrows -> [Transformed] {
        var transformed: [Transformed] = []
        for element in self {
            transformed.append(try transform(element))
        }
        return transformed
    }
}

zip(0..<Int.max,names).forEach { print($0, $1) }

// map
// sorted
// reversed
// enumerated
// indices
// zip
// shuffled
// partition
