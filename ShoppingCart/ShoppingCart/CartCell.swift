//
//  CartCell.swift
//  ShoppingCart
//
//  Created by Joshua Homann on 2/22/19.
//  Copyright © 2019 com.josh. All rights reserved.
//

import UIKit

class CartCell: UITableViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var totalLabel: UILabel!
    @IBOutlet var plusButton: UIButton!
    @IBOutlet var quantityLabel: UILabel!
    static let reuseIdentifier = String(describing: CartCell.self)
    private var tapPlus: (()->())?
    func configure(with cartItem: CartItem, tapPlus: @escaping ()->()) {
        nameLabel.text = cartItem.title
        totalLabel.text = "\(cartItem.price)"
        quantityLabel.text = "\(cartItem.quantity)"
        self.tapPlus = tapPlus
    }
    @IBAction private func tapPlus(_ sender: Any) {
        tapPlus?()
    }
}
