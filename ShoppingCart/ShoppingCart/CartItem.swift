//
//  CartItem.swift
//  ShoppingCart
//
//  Created by Joshua Homann on 2/22/19.
//  Copyright © 2019 com.josh. All rights reserved.
//

import Foundation

struct CartItem {
    var title: String
    var quantity: Int
    var price: Decimal

    static let samples: [CartItem] = [
        .init(title: "Widget", quantity: 1, price: 5),
        .init(title: "Cog", quantity: 1, price: 3),
        .init(title: "Thingy", quantity: 1, price: 2),
        ]
}
