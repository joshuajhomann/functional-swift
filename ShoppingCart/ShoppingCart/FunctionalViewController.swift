//
//  FunctionalViewController.swift
//  ShoppingCart
//
//  Created by Joshua Homann on 2/22/19.
//  Copyright © 2019 com.josh. All rights reserved.
//

import UIKit

class FunctionalViewController: UIViewController {
    @IBOutlet private var tableView: UITableView!
    @IBOutlet var totalLabel: UILabel!
    private var cartItems: [CartItem] = [] {
        didSet {
            view.setNeedsLayout()
        }
    }

    private lazy var currencyFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        view.setNeedsLayout()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        totalLabel.text = FunctionalViewController.cartTotalText(from: cartItems, formatter: currencyFormatter)
        tableView.reloadData()
    }

    @IBAction func tapPlus(_ sender: Any) {
        let item = CartItem.samples.randomElement()!
        cartItems.append(item)
    }
}

// MARK: Stateless
extension FunctionalViewController {
    static func cartTotalText(from cartItems: [CartItem], formatter: NumberFormatter) -> String? {
        var total: Decimal = 0
        for item in cartItems {
            total += item.price * Decimal(item.quantity)
        }
        return formatter.string(for: total)
    }
}

extension FunctionalViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartItems.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CartCell.reuseIdentifier, for: indexPath)
        (cell as? CartCell)?.configure(with: cartItems[indexPath.row]) { [weak self] in
            self?.cartItems[indexPath.row].quantity += 1
        }
        return cell
    }
}

extension FunctionalViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        cartItems.remove(at: indexPath.row)
    }
}
