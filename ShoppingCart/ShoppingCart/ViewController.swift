//
//  ViewController.swift
//  ShoppingCart
//
//  Created by Joshua Homann on 2/22/19.
//  Copyright © 2019 com.josh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet private var tableView: UITableView!
    @IBOutlet var totalLabel: UILabel!
    private var cartItems: [CartItem] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        updateTotalLabel()
    }

    private func updateTotalLabel() {
        var total: Decimal = 0
        for item in cartItems {
            total += item.price * Decimal(item.quantity)
        }
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        totalLabel.text = formatter.string(for: total)
    }

    @IBAction func tapPlus(_ sender: Any) {
        let item = CartItem.samples.randomElement()!
        cartItems.append(item)
        tableView.reloadData()
        updateTotalLabel()
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartItems.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CartCell.reuseIdentifier, for: indexPath)
        (cell as? CartCell)?.configure(with: cartItems[indexPath.row]) { [weak self] in
            self?.cartItems[indexPath.row].quantity += 1
            self?.tableView.reloadRows(at: [indexPath], with: .automatic)
            self?.updateTotalLabel()
        }
        return cell
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        cartItems.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .automatic)
        updateTotalLabel()
    }
}

