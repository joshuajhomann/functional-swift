//
//  ShoppingCartTests.swift
//  ShoppingCartTests
//
//  Created by Joshua Homann on 2/22/19.
//  Copyright © 2019 com.josh. All rights reserved.
//

import XCTest
import UIKit
@testable import ShoppingCart

class ShoppingCartTests: XCTestCase {
    private let items: [[CartItem]] =
        [
            [
                .init(title: "", quantity: 5, price: 0.50)
            ],
            [
                .init(title: "", quantity: 5, price: 0.50),
                .init(title: "", quantity: 10, price: 1.00)
            ],
            [
                .init(title: "", quantity: 5, price: 1000.00),
            ],
        ]
    func testUSTotal() {
        let usCurrencyFormatter = NumberFormatter()
        usCurrencyFormatter.numberStyle = .currency
        usCurrencyFormatter.locale = Locale(identifier: "EN-US")

        XCTAssertTrue(
            FunctionalViewController.cartTotalText(
                from: items[0],
                formatter: usCurrencyFormatter) == "$2.50"
        )

        XCTAssertTrue(
            FunctionalViewController.cartTotalText(
                from: items[1],
                formatter: usCurrencyFormatter) == "$12.50"
        )

        XCTAssertTrue(
            FunctionalViewController.cartTotalText(
                from: items[2],
                formatter: usCurrencyFormatter) == "$5,000.00"
        )
    }
    
    func testUKTotal() {
        let ukCurrencyFormatter = NumberFormatter()
        ukCurrencyFormatter.numberStyle = .currency
        ukCurrencyFormatter.locale = Locale(identifier: "EN-UK")
        XCTAssertTrue(
            FunctionalViewController.cartTotalText(
                from: items[0],
                formatter: ukCurrencyFormatter) == "£2.50"
        )

        XCTAssertTrue(
            FunctionalViewController.cartTotalText(
                from: items[1],
                formatter: ukCurrencyFormatter) == "£12.50"
        )

        XCTAssertTrue(
            FunctionalViewController.cartTotalText(
                from: items[2],
                formatter: ukCurrencyFormatter) == "£5,000.00"
        )
    }


}
