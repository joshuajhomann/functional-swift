import UIKit

//joined
//split
//flatMap
//+

let a = [[1,2,3], [4,5]]
let names = "S sasklj kajlskj jjlj"

let s = names.split(separator: " ")
s.joined(separator: ",")

let b = a.flatMap {$0}
print(b)

let zs: String? = "cnn.com"
let url = zs.flatMap(URL.init(string:))
